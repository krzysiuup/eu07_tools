from .input import load, RailProfileInput
from .structures import RailProfile, RailProfilePoint