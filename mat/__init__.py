from .input import load
from .common import NESTED_MAPPING_KEYS, NUMERIC_KEYS, get_alias_from_key, get_texture_index_from_key
from .structures import create_mapping